package com.yuanyi.property.demo.client;

import com.yuanyi.property.demo.entity.Test;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Demo2ClientHystric implements Demo2Client {

    private Logger log = Logger.getLogger(Demo2ClientHystric.class);

    @Override
    public List<Test> list() {
        log.info("进入列表!");
        throw new RuntimeException("list 保存失败.");
    }

    @Override
    public int save() {
        log.info("进入save熔断器!");
        throw new RuntimeException("save 保存失败.");
    }
}
