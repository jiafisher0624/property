package com.yuanyi.property.demo.dao;


import com.yuanyi.property.demo.entity.Test;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import java.util.List;

/**
 * Created by lorne on 2017/6/28.
 */
@Mapper
public interface TestMapper {


    @Select("SELECT * FROM test.T_TEST")
    List<Test> findAll();

    @Insert("INSERT INTO test.T_TEST(NAME) VALUES(#{name})")
    int save(@Param("name") String name);

}
